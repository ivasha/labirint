﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LocalizationManager : MonoBehaviour
{

    public static LocalizationManager instance;
    public string fileName;

    private Dictionary<string, string> localizedText;
    private bool isReady = false;
    private string missingTextString = "Localized text not found";

    void Awake()
    {
       instance = this;
       DontDestroyOnLoad(gameObject);
       LoadLocalizedText(fileName);
    }
    
    public void LoadLocalizedText(string fileName)
    {
        localizedText = new Dictionary<string, string>();
        string jsonString;
        LocalizationData loadedData;

        string filePath = Path.Combine(Application.streamingAssetsPath, fileName);

        #if UNITY_EDITOR
                if (File.Exists(filePath))
                {
                    string dataAsJson = File.ReadAllText(filePath);
                    loadedData = JsonUtility.FromJson<LocalizationData>(dataAsJson);
                }
        #endif

        #if UNITY_ANDROID
                WWW reader = new WWW(filePath);
                while (!reader.isDone) { }

                jsonString = reader.text;
        #endif

        loadedData = JsonUtility.FromJson<LocalizationData>(jsonString);

        for (int i = 0; i < loadedData.items.Length; i++)
        {
            localizedText.Add(loadedData.items[i].key, loadedData.items[i].value);
        }
        isReady = true;
    }

    public string GetLocalizedValue(string key)
    {
        string result = missingTextString;
        if (localizedText.ContainsKey(key))
        {
            result = localizedText[key];
        }

        return result;

    }

    public bool GetIsReady()
    {
        return isReady;
    }

}