﻿using UnityEngine;

public class Wall : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.gameObject.CompareTag("Ball")) return;
        Debug.Log(other.gameObject.name);
        other.gameObject.GetComponent<Ball>().SetStartPosition();
        InputController.Instance.MoveAllowed = false;
    }
}
