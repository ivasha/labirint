﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Ball : MonoBehaviour, IPointerDownHandler
{
    public float speed = 0.005f;
    public static Ball Instance;

    private Vector3 _startPosition;
    private Button _btn;
    public Vector3 StartPosition { get { return _startPosition; } }

    private void Awake()
    {
        Instance = this;
    }

    void Start() {
        Debug.Log(gameObject.transform.parent);
        _startPosition = transform.position;
        _btn = GetComponent<Button>();
        Finish.Levelup += Finish_Levelup;
        Menu.Back += Menu_Back;
        InputController.MoveBall += InputController_MoveBall;     
    }

    private void Menu_Back()
    {
        SetStartPosition();
    }

    private void Finish_Levelup()
    {
        SetStartPosition();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        InputController.Instance.BallPressed();
    }

    private void InputController_MoveBall(Vector3 mousePosition)
    {
        mousePosition.z = 0;
        transform.position = Vector3.Lerp(transform.position, mousePosition, speed * Time.deltaTime);
    }

    public void SetStartPosition()
    {
        transform.position = _startPosition;
        InputController.Instance.MoveAllowed = false;
        InputController.Instance.Pressed = false;

    }
}
