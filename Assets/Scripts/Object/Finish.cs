﻿using UnityEngine;

public class Finish : MonoBehaviour
{
    public delegate void  LevelUpHandler();
    public static event LevelUpHandler Levelup;

    private Ball ball;

    private void Awake()
    {
        ball = FindObjectOfType<Ball>();
    }

    private void Update()
    {
        if (LevelLoader.Instance.Level != 3) return;
        float dist = Vector3.Distance(ball.transform.position, transform.position);

        float screamerDist = Random.Range(500, 1500);

        if (dist > screamerDist) return;

        if (Levelup == null) return;

        Levelup();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.gameObject.CompareTag("Ball")) return;

        if (Levelup == null) return;
        Levelup();
    }
}
