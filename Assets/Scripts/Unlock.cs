﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unlock : MonoBehaviour {

    private void Awake()
    {
        PlayerPrefs.SetString("Unlock", "NO");
        UnityAdsScript.Unlock += UnityAdsScript_Unlock;
    }

    // Use this for initialization
    void Start () {
        if (PlayerPrefs.GetString("Unlock").Equals("NO"))
        {
            this.gameObject.SetActive(true);
        }
        else
        {
            this.gameObject.SetActive(false);
        }
    }


    private void UnityAdsScript_Unlock()
    {
        this.gameObject.SetActive(false);
    }

}
