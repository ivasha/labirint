﻿using GoogleMobileAds.Api;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdMobBanner : MonoBehaviour {

    public static AdMobBanner Instance;
    private BannerView bannerAd;

    private void Awake()
    {
        Instance = this;
        ShowBannerAd();

    }

    public void ShowBannerAd()
    {
        string adID = "ca-app-pub-6174908755783369/2022695136";
        AdRequest request = new AdRequest.Builder().Build();

        bannerAd = new BannerView(adID, AdSize.SmartBanner, AdPosition.Bottom);
        bannerAd.LoadAd(request);
    }

    private void BannerAd_OnAdLoaded(object sender, System.EventArgs e)
    {
        bannerAd.Show();
    }

    public void HideBanner()
    {
        //  bannerAd.Hide();
    }

    public void ShowBanner()
    {
        //bannerAd.Show();
    }
}

