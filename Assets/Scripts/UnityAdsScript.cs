﻿using Assets.Scripts;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;


public class UnityAdsScript: MonoBehaviour
{
    public Text message;
    public static UnityAdsScript Instance;
    public delegate void UnlockHandler();
    public static event UnlockHandler Unlock;

   private Button _btnUnlock;

    private void Awake()
    {
        Instance = this;
        _btnUnlock = GetComponent<Button>();
        _btnUnlock.onClick.AddListener(ShowAd);
    }


    void Start()
    {
        if (Advertisement.isSupported)
        {
            Advertisement.Initialize("1166821");
        }
    }

    private void Update()
    {
      //  if (_btnUnlock) _btnUnlock.interactable = Advertisement.IsReady();

        if (!Advertisement.IsReady())
        {
            _btnUnlock.interactable = false;
            message.gameObject.SetActive(true);

        } else
        {
            _btnUnlock.interactable = true;
            message.gameObject.SetActive(false);
        }
    }


    public void ShowAd()
    {
        if (Advertisement.IsReady())
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                PlayerPrefs.SetString("Unlock", "YES");
                Menu.Instance.OpenMenu((int)ScreenGame.Menu);
                if (Unlock != null) Unlock();
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }
}