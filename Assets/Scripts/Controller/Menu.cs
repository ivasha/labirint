﻿using Assets.Scripts;
using System.Collections;
using UnityEngine;

public class Menu : MonoBehaviour {

    public AudioSource _audioClick;
    public GameObject[] Panels;
    public GameObject UIElement;
    public GameObject[] Levels;

    public static Menu Instance;
    private int _currentPanel = 0;
    private int _currLevel = 0;

    public delegate void BackHandler();
    public static event BackHandler Back;

    public int ScreenCurrent { get { return _currentPanel; }  }

    private void Awake () {
        Instance = this;
    }

    private IEnumerator Start()
    {
        while (!LocalizationManager.instance.GetIsReady())
        {
            yield return null;
        }
    }
    
    public void OpenMenu(int panelToOpen)
    {
        _audioClick.Play();
        if (_currentPanel == panelToOpen) return;

        DisabledPanel(Levels[_currLevel]);
        DisabledPanel(Panels[_currentPanel]);
        EnabledPanel(Panels[panelToOpen]);

        if (Back != null)
        {
            Back();
        }

        _currentPanel = panelToOpen;
        
        if (_currentPanel == (int ) ScreenGame.Start || _currentPanel == (int)ScreenGame.Dialog)
        {
            DisabledPanel(UIElement);
            AdMobBanner.Instance.HideBanner();
            _currLevel = 0;
        } else
        {
            AdMobBanner.Instance.ShowBanner();
            EnabledPanel(UIElement);
        }
    }
    
    public void OpenLevel()
    {
        _audioClick.Play();

        int lvl = LevelLoader.Instance.Level-1;
        
        DisabledPanel(Panels[_currentPanel]);
        DisabledPanel(Levels[_currLevel]);
        EnabledPanel(Levels[lvl]);
        _currLevel = lvl;
        LevelLoader.Instance.LoadLevel(Levels[_currLevel]);
    }

    public void EnabledPanel(GameObject panel)
    {
        panel.gameObject.SetActive(true);
    }

    public void DisabledPanel(GameObject panel)
    {
        panel.gameObject.SetActive(false);
    }
}
