﻿using Assets.Scripts;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LevelLoader : MonoBehaviour {

    public AudioSource AudioBack;
    public static LevelLoader Instance;

    private int _level = 1;
    private GameObject _lvl;
    private string _version;

    public int Level { get { return _level; } }

    private void Awake()
    {
        Instance = this;
    }

    void Start () {
        Finish.Levelup += Finish_Levelup;
        Menu.Back += Menu_Back;
    }

    private void Menu_Back()
    {
        ResetLevel();
    }

    private void Finish_Levelup()
    {
        IncreaseLvl();
        GameObject lvlPanel = Menu.Instance.Levels[_level-1];
        LoadLevel(lvlPanel);
        Menu.Instance.OpenLevel();
    }

    public void SetVersion(string version)
    {
        _version = version;
    }
    public void LoadLevel(GameObject lvl)
    {
        if (_version.Equals(null)) return;
        if (_level == 4)
        {
            Ball.Instance.SetStartPosition();
            StartCoroutine(Scream(lvl));
            return;
        }
        lvl.GetComponent<Image>().sprite = Instantiate(Resources.Load<Sprite>("Items/Panels/Levels/" + _version + "/" + _level));

    }

    IEnumerator Scream(GameObject lvl)
    {
        AudioBack.Stop();
        lvl.GetComponent<AudioSource>().Play();

        yield return new WaitForSeconds(3);
        Menu.Instance.OpenMenu((int)ScreenGame.Start);
    }

    private void IncreaseLvl()
    {
        _level += 1;
    }

    private void ResetLevel()
    {
        _level = 1;
    }
}
