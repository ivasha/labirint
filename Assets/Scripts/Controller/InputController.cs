﻿using System;
using UnityEngine;

public class InputController : MonoBehaviour {


    public delegate void MoveBallHandler(Vector3 mousePosition);
    public static event MoveBallHandler MoveBall;

    public static InputController Instance;

    public bool isToch = false;

    private bool _pressed = false;
    private RaycastHit2D hit;
    private Vector3 _touchPos;
    private bool _moveAllowed = false;

    public bool MoveAllowed { get { return _moveAllowed; } set {_moveAllowed = value; } }
    public bool Pressed { set { _pressed = value; } }

    private void Awake()
    {
        Instance = this;
    }

    private void Update()
    {
        CheckTouch();
    }

    private void CheckTouch()
    {
        if (Input.touchCount > 0)
        {
            isToch = true;
            Touch touch = Input.GetTouch(0);
            Vector3 touchPos = touch.position;
            touchPos.z = 0;

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    if (_pressed)
                    {
                        _moveAllowed = true;
                    }
                   
                    break;
                case TouchPhase.Moved:
                    if ( _moveAllowed && _pressed)
                    {
                        if (MoveBall == null) return;
                        MoveBall(touchPos);
                    }
                    break;
                case TouchPhase.Ended:
                    _moveAllowed = false;
                    _pressed = false;
                    break;
            }
        }
    }

    public void BallPressed()
    {
        _pressed = true;
    }
}
