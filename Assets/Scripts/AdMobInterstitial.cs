﻿using GoogleMobileAds.Api;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdMobInterstitial : MonoBehaviour {

    private InterstitialAd interstitial;

    // Use this for initialization
    void Start()
    {
        RequestInterstitialAds();
        Menu.Back += Menu_Back;
    }

    private void Menu_Back()
    {
        if (InputController.Instance.isToch)
        {
            Debug.Log("TOUCH");
            InputController.Instance.isToch = false;
            Invoke("showInterstitialAd", 3f);
        }
    }

    public void showInterstitialAd()
    {
        //Show Ad
        if (interstitial.IsLoaded())
        {
            interstitial.Show();

            Debug.Log("SHOW AD XXX");
        }
    }

    private void RequestInterstitialAds()
    {
        string adID = "ca-app-pub-6174908755783369/3499428332";

#if UNITY_ANDROID
        string adUnitId = adID;
#elif UNITY_IOS
        string adUnitId = adID;
#else
        string adUnitId = adID;
#endif

        interstitial = new InterstitialAd(adUnitId);
        AdRequest request = new AdRequest.Builder().Build();
        interstitial.OnAdClosed += Interstitial_OnAdClosed; ;

        interstitial.LoadAd(request);

        Debug.Log("AD LOADED XXX");
    }

    private void Interstitial_OnAdClosed(object sender, System.EventArgs e)
    {
        throw new System.NotImplementedException();
    }
}
